- mc20* are reprocessed mc16* 
- mc20a, mc20d, mc20e correspond to the different profiles of number of interactions for data15-16, data17 and data18.
- mc21a/md23a correspond to the different profiles of number of interactions for data22.
- This is encoded in some reoccurring r-tags

| mc campaign | r-tag  |
| ----------- | ------ |
| mc20a       | r13167 |
| mc20d       | r13144 |
| mc20e       | r13145 |
| mc21a       | r13829 |
| mc23a       | r14622 |
| mc23d       | r15224 |
| mc23e       | r15919 |

