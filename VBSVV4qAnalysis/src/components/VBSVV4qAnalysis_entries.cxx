#include "../VBSJetsSelectorAlg.h"
#include "../SignalJetsSelectorAlg.h"
#include "../BaselineVarsJJAlg.h"
#include "../JJSelectorAlg.h"
#include "../TriggerDecoratorAlg.h"

using namespace VBSVV4q;

DECLARE_COMPONENT(VBSJetsSelectorAlg)
DECLARE_COMPONENT(SignalJetsSelectorAlg)
DECLARE_COMPONENT(TriggerDecoratorAlg)
DECLARE_COMPONENT(JJSelectorAlg)
DECLARE_COMPONENT(BaselineVarsJJAlg)
